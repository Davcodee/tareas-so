## Tareas - Sistemas Operativos - 2020-1

![Build Status](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2020-1/tareas-so/badges/master/build.svg)

En este repositorio estaremos manejando las tareas de la materia de [sistemas operativos][url-sistemas_operativos-gitlab] que se imparte en la [Facultad de Ciencias, UNAM][url-sistemas_operativos-fciencias] en el semestre 2020-1.

### Estructura de directorios

+ Cada alumno deberá tener una carpeta dentro de la que hará entrega de sus tareas y prácticas
+ Si la práctica es en equipo, uno sube la carpeta de la práctica y los demás entregan una liga simbólica a la carpeta correspondiente de la práctica.
+ Cada tarea o práctica deberá tener lo siguiente:
    * Un archivo `README.md` donde se entregará la documentación en formato _markdown_. Se puede hacer uso de un directorio `img/` para guardar las imágenes o capturas necesarias para la documentación
    * Un archivo `Makefile` que servirá para compilar el programa y hacer pruebas

```
content/tarea/AndresHernandez/
├── .gitkeep
├── README.md
├── tareas
│   ├── tarea-0/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
│   ├── tarea-1/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   ├── Makefile
│   │   ├── programa.c
│   │   └── ...
│   ├── ...
│   └── tarea-n/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
└── practicas/
    ├── practica-1/
    │   ├── img/
    │   │   └── ...
    │   ├── README.md
    │   └── ...
    ├── practica-2/ -> ../../JuanCamacho/practicas/practica-2/
    ├── ...
    └── practica-m/
        ├── img/
        │   └── ...
        ├── README.md
        └── ...
```

--------------------------------------------------------------------------------

##### Vista en GitLab

+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2020-1/tareas-so>

##### Vista web

+ <https://SistemasOperativos-Ciencias-UNAM.gitlab.io/2020-1/tareas-so/>

--------------------------------------------------------------------------------

[url-sistemas_operativos-gitlab]: https://SistemasOperativos-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[url-sistemas_operativos-fciencias]: "http://www.fciencias.unam.mx/docencia/horarios/20201/1556/713" "Sistemas Operativos 2020-1"

